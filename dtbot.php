<?php
function DolarTodayBot() 
{
	include('config.php'); // $botToken = "123456:ABC-DEF1234ghIkl-zyx57W2v1u123ew11";
	$website = "https://api.telegram.org/bot".$botToken;
	try
	{
		if (!($content = @file_get_contents("php://input")) === false) 
		{
			$json = json_decode($content, TRUE);
			$message = $json["message"];
			$chat_id = $message["chat"]["id"];
			$first_name = $message["from"]["first_name"];
			$username = $message["from"]["username"];
			$text = $message["text"];

			//Funcion opcional para guardar cada consulta al bot
			include('database.php'); 
			if(function_exists('saveHook'))
				saveHook($content, $chat_id, $username, $first_name, $text);

			if($text === "/start" || $text === "/start@DolarBsFBot") 
			{
				$text_send = urlencode("*Bienvenido!*\nPara obtener el precio un dólar en bolívares: /dolar\nPara suscribirse a notificaciones cuando cambie el precio: /subscribe\nEstos datos son obtenidos de la página [DolarToday.com](https://bit.ly/venezuela911)");
				file_get_contents($website."/sendmessage?chat_id=".$chat_id."&parse_mode=Markdown&text=".$text_send);
			}
			elseif($text === "/dolar" || $text === "/dolar@DolarBsFBot") 
			{
				$json = @file_get_contents("https://s3.amazonaws.com/dolartoday/data.json");
				$details = json_decode(utf8_encode($json), TRUE);
				$dolar = $details["USD"]["dolartoday"];
				$text_send = urlencode("Valor de USD/BsF\n*USD:* 1\n*BsF:* ".$dolar."\n[DolarToday](https://bit.ly/venezuela911)");
				file_get_contents($website."/sendmessage?chat_id=".$chat_id."&parse_mode=Markdown&text=".$text_send);
			}
			elseif($text === "/subscribe" || $text === "/subscribe@DolarBsFBot") 
			{
				if(function_exists('subscribe'))
				{
					subscribe($chat_id);
					$text_send = urlencode("*Ya estas suscrito!*\nLas notificaciones se enviarán cada hora si el precio cambia.");
					file_get_contents($website."/sendmessage?chat_id=".$chat_id."&parse_mode=Markdown&text=".$text_send);
				}
			}
			elseif($text === "/unsubscribe" || $text === "/unsubscribe@DolarBsFBot") 
			{
				if(function_exists('unsubscribe'))
				{
					unsubscribe($chat_id);
					file_get_contents($website."/sendmessage?chat_id=".$chat_id."&parse_mode=Markdown&text=Desuscrito :(");
				}
			}
			else
			{
				$text_send = urlencode("Para obtener el precio un dólar en bolívares: /dolar\nPara suscribirse a notificaciones cuando cambie el precio: /subscribe\nEstos datos son obtenidos de la página [DolarToday.com](https://bit.ly/venezuela911)");
				file_get_contents($website."/sendmessage?chat_id=".$chat_id."&parse_mode=Markdown&text=".$text_send);
			}

		}
	}
	catch(Exception $e)
	{
		echo $e->getMessage();
	}
}

DolarTodayBot();
